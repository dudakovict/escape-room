using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DoorScript : MonoBehaviour
{
    private Animator Anim;
    public bool IsOpen = false;
    public bool IsLocked;
    public string DoorType;
    private AudioSource AudioPlayer;
    [SerializeField] AudioClip CreakingWoodenDoor;
    [SerializeField] AudioClip CreakingMetalDoor;
    [SerializeField] AudioClip OldWoodenDoor;
    [SerializeField] bool CWD;
    [SerializeField] bool CMD;
    [SerializeField] bool OWD;
    [SerializeField] bool BasementDoor;

    // Start is called before the first frame update
    void Start()
    {
        Anim = GetComponent<Animator>();
        AudioPlayer = GetComponent<AudioSource>();
    }

    // Update is called once per frame
    void Update()
    {
        // Currently moving an object
        if (SaveScript.HoldingObjectID != -1) return;

        if (BasementDoor)
        {
            DoorType = "Basement";
            if (SaveScript.Keys[2].GetActive())
            {
                IsLocked = false;
            }
        }
        if(DoorType.Equals("KitchenMidDoor"))
        {
            if (SaveScript.Keys[0].GetActive())
            {
                IsLocked = false;
            }
        }
        else if (DoorType.Equals("HallDoor"))
        {
            if (SaveScript.ElectricityBoxExploded)
            {
                IsLocked = false;
            }
        }
        else if (DoorType.Equals("GuardRoom"))
        {
            if (SaveScript.Keys[3].GetActive())
            {
                IsLocked = false;
            }
        }
    }

    public void DoorOpen()
    {
        IsLocked = false;

        if (!IsOpen)
        {
            Anim.SetTrigger("Open");
            IsOpen = true;
        }
        else if (IsOpen)
        {
            Anim.SetTrigger("Close");
            IsOpen = false;
        }

        PlayDoorSound(CWD, CMD, OWD);
    }

    public void PlayDoorSound(bool CWD, bool CMD, bool OWD)
    {
        if (CWD)
        {
            AudioPlayer.clip = CreakingWoodenDoor;
        }
        else if (CMD)
        {
            AudioPlayer.clip = CreakingMetalDoor;
        }
        else
        {
            AudioPlayer.clip = OldWoodenDoor;
        }

        AudioPlayer.Play();
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Enemy"))
        {
            if (!IsLocked)
            {
                if (!IsOpen)
                {
                    Anim.SetTrigger("Open");
                    IsOpen = true;
                }
            }
        }
    }
}
