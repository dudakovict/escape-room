using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DamageMetalProblemOne : MonoBehaviour
{
    public AudioSource StabPlayer;

    private Camera mainCamera;

    public UIMessageSystem MessageSystem;

    // Start is called before the first frame update
    void Start()
    {
        mainCamera = Camera.main.GetComponent<Camera>();
    }

    public void HandleBreakPlane()
    {
        if (!SaveScript.StartingAnimationFinished) return;
        if (SaveScript.InInspectMode || SaveScript.InInventoryMode || SaveScript.InOptionsMode || SaveScript.InKeypadMode) return;

        MessageSystem.DisableAll();
        MessageSystem.EnableSanity();
        MessageSystem.EnableCouldBreakMessage();
    }

    private void OnMouseExit()
    {
        if (Time.deltaTime == 0) return;

        MessageSystem.EnableCrosshair();
        MessageSystem.DisableCouldBreakMessage();
    }

    private void OnTriggerEnter(Collider other)
    {
        int idx = Weapon.GetWeaponIndex(SaveScript.Weapons, other.gameObject.tag);
        if (idx != -1)
        {
            SaveScript.SecretTunnelDestroyed = true;
            StabPlayer.Play();
            if (gameObject.CompareTag("Door"))
            {
                SaveScript.OfficeDoorDestroyed = true;
            }
            Destroy(gameObject);
            MessageSystem.EnableCrosshair();
            MessageSystem.DisableCouldBreakMessage();
        }
    }
}
