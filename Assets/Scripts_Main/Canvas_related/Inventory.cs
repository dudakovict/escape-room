using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public class Inventory : MonoBehaviour
{
    [SerializeField] GameObject InventoryMenu;
    private AudioSource AudioPlayer;
    [SerializeField] Animator Anim;
    [SerializeField] AudioClip AppleBite;
    [SerializeField] AudioClip BatteryChange;
    [SerializeField] AudioClip MeeleWeaponChange;
    [SerializeField] AudioClip GunWeaponChange;

    [SerializeField] List<GameObject> AppleIcons = new List<GameObject>();
    [SerializeField] List<GameObject> AppleButtons = new List<GameObject>();
    [SerializeField] List<GameObject> BatteryIcons = new List<GameObject>();
    [SerializeField] List<GameObject> BatteryButtons = new List<GameObject>();
    [SerializeField] List<GameObject> WeaponIcons = new List<GameObject>();
    [SerializeField] List<GameObject> WeaponButtons = new List<GameObject>();
    [SerializeField] List<GameObject> KeyIcons = new List<GameObject>();
    [SerializeField] List<GameObject> KeyButtons = new List<GameObject>();
    [SerializeField] List<GameObject> AmmoIcons = new List<GameObject>();
    [SerializeField] List<GameObject> AmmoButtons = new List<GameObject>();
    [SerializeField] List<GameObject> Weapons = new List<GameObject>();
    [SerializeField] GameObject PlayerArms;
    [SerializeField] GameObject OptionsMenu;
    [SerializeField] GameObject GunUI;
    [SerializeField] GameObject BulletsUI;
    [SerializeField] UIMessageSystem messageSystem;
    [SerializeField] GameObject FPSDisplay;

    void Start()
    {
        InitializeComponent();
        InitializeInventory();
    }

    void Update()
    {
        if (!SaveScript.StartingAnimationFinished) return;

        ToggleInventory();
        ToggleOptions();
        CheckApples();
        CheckBatteries();
        CheckWeapons();
        CheckKeys();
        CheckAmmo();
    }

    private void ToggleInventory()
    {
        if (SaveScript.InOptionsMode || 
            SaveScript.InInspectMode || 
            SaveScript.HoldingObjectID != -1 ||
            SaveScript.InKeypadMode) return;

        if (Input.GetKeyDown(KeyCode.I))
        {
            messageSystem.DisableAll();
            messageSystem.EnableSanity();
            messageSystem.EnableCrosshair();

            InventoryMenu.gameObject.SetActive(!InventoryMenu.gameObject.activeSelf);

            Time.timeScale = (Time.timeScale == 1.0f) ? 0.0f : 1.0f;
            
            if (Cursor.lockState == CursorLockMode.Locked)
                Cursor.lockState = CursorLockMode.Confined;
            else Cursor.lockState = CursorLockMode.Locked;

            if (Cursor.lockState == CursorLockMode.Confined) Cursor.visible = true;
            else Cursor.visible = false;

            if (Time.timeScale == 1f)
            {
                SaveScript.InInventoryMode = false;
                PlayerArms.GetComponent<PlayerAttacks>().enabled = true;
                FPSDisplay.SetActive(true);
            }
            if (Time.timeScale == 0f)
            {
                SaveScript.InInventoryMode = true; 
                PlayerArms.GetComponent<PlayerAttacks>().enabled = false;
                FPSDisplay.SetActive(false);
            }
        }
    }

    private void ToggleOptions()
    {
        if (SaveScript.InInventoryMode || 
            SaveScript.InInspectMode || 
            SaveScript.HoldingObjectID != -1 ||
            SaveScript.InKeypadMode) return;

        if (Input.GetKeyDown(KeyCode.O) || Input.GetKeyDown(KeyCode.Escape))
        {
            messageSystem.DisableAll();
            messageSystem.EnableSanity();
            messageSystem.EnableCrosshair();

            OptionsMenu.gameObject.SetActive(!OptionsMenu.gameObject.activeSelf);
            Time.timeScale = (Time.timeScale == 1.0f) ? 0.0f : 1.0f;
            
            if (Cursor.lockState == CursorLockMode.Locked)
                Cursor.lockState = CursorLockMode.Confined;
            else Cursor.lockState = CursorLockMode.Locked;

            if (Cursor.lockState == CursorLockMode.Confined) Cursor.visible = true;
            else Cursor.visible = false;

            if (Time.timeScale == 1f)
            {
                SaveScript.InOptionsMode = false;
                PlayerArms.GetComponent<PlayerAttacks>().enabled = true;
                FPSDisplay.SetActive(true);
            }
            if (Time.timeScale == 0f)
            {
                SaveScript.InOptionsMode = true;
                PlayerArms.GetComponent<PlayerAttacks>().enabled = false;
                FPSDisplay.SetActive(false);
            }
        }
    }

    private void InitializeComponent()
    {
        InventoryMenu.gameObject.SetActive(false);
        OptionsMenu.gameObject.SetActive(false);
        GunUI.gameObject.SetActive(false);
        BulletsUI.gameObject.SetActive(false);
        AudioPlayer = GetComponent<AudioSource>();
        Cursor.visible = false;
        Time.timeScale = 1.0f;
    }

    private void InitializeInventory()
    {
        for (var i = 0; i < 6; i++)
        {
            AppleIcons[i].gameObject.SetActive(false);
            AppleButtons[i].gameObject.SetActive(false);
        }

        for (var i = 0; i < 4; i++)
        {
            BatteryIcons[i].gameObject.SetActive(false);
            BatteryButtons[i].gameObject.SetActive(false);
        }

        for (var i = 0; i < 5; i++)
        {
            WeaponIcons[i].gameObject.SetActive(false);
            WeaponButtons[i].gameObject.SetActive(false);
        }

        for (var i = 0; i < 4; i++)
        {
            KeyIcons[i].gameObject.SetActive(false);
            KeyButtons[i].gameObject.SetActive(false);
        }

        for (var i = 0; i < 4; i++)
        {
            AmmoIcons[i].gameObject.SetActive(false);
            AmmoButtons[i].gameObject.SetActive(false);
        }
    }

    private void CheckApples()
    {
        for (var i = 0; i < SaveScript.Apples; i++)
        {
            AppleIcons[i].gameObject.SetActive(true);
            AppleButtons[i].gameObject.SetActive(true);
        }

        for (var i = SaveScript.Apples; i < 6; i++)
        {
            AppleIcons[i].gameObject.SetActive(false);
            AppleButtons[i].gameObject.SetActive(false);
        }
    }

    private void CheckBatteries()
    {
        for (var i = 0; i < SaveScript.Batteries; i++)
        {
            BatteryIcons[i].gameObject.SetActive(true);
            BatteryButtons[i].gameObject.SetActive(true);
        }

        for (var i = SaveScript.Batteries; i < 4; i++)
        {
            BatteryIcons[i].gameObject.SetActive(false);
            BatteryButtons[i].gameObject.SetActive(false);
        }
    }

    private void CheckAmmo()
    {
        for (var i = 0; i < SaveScript.BulletClips; i++)
        {
            AmmoIcons[i].gameObject.SetActive(true);
            AmmoButtons[i].gameObject.SetActive(true);
        }

        for (var i = SaveScript.BulletClips; i < 4; i++)
        {
            AmmoIcons[i].gameObject.SetActive(false);
            AmmoButtons[i].gameObject.SetActive(false);
        }
    }

    private void CheckWeapons()
    {
        for (var i = 0; i < SaveScript.Weapons.Length; i++)
        {
            if (SaveScript.Weapons[i].GetActive())
            {
                WeaponIcons[i].gameObject.SetActive(true);
                WeaponButtons[i].gameObject.SetActive(true);
            }
        }
    }

    private void CheckKeys()
    {
        for (var i = 0; i < SaveScript.Keys.Length; i++)
        {
            if (SaveScript.Keys[i].GetActive())
            {
                KeyIcons[i].gameObject.SetActive(true);
                KeyButtons[i].gameObject.SetActive(true);
            }
        }
    }

    public void SanityUpdate()
    {
        if (SaveScript.PlayerSanity < 100)
        {
            if (SaveScript.PlayerSanity < 90)
            {
                SaveScript.PlayerSanity += 10;
            }
            else
            {
                SaveScript.PlayerSanity = 100;
            }
            SaveScript.SanityChanged = true;
            SaveScript.Apples--;
            AudioPlayer.clip = AppleBite;
            AudioPlayer.Play();
        }
    }

    public void BatteryUpdate()
    {
        if (SaveScript.BatteryPower < 1.0f)
        {
            SaveScript.BatteryPower = 1.0f;
            SaveScript.BatteryRefill = true;
            SaveScript.Batteries--;
            AudioPlayer.clip = BatteryChange;
            AudioPlayer.Play();
        }
    }

    public void BulletsRefill()
    {
        SaveScript.BulletClips--;
        SaveScript.Bullets = 6;
        AudioPlayer.clip = GunWeaponChange;
        AudioPlayer.Play();

    }

    public void WeaponUpdate(string tag)
    {
        // Remove weapon by clicking the same icon
        if (SaveScript.PreviouslyEquippedWeaponTag.Equals(tag))
        {
            SaveScript.PreviouslyEquippedWeaponTag = "";
            PlayerArms.gameObject.SetActive(false);
            AudioPlayer.Play();
            return;
        }
        SaveScript.PreviouslyEquippedWeaponTag = tag;

        PlayerArms.gameObject.SetActive(true);
        foreach (string key in SaveScript.EquippedWeapon.Keys.ToList())
        {
            SaveScript.EquippedWeapon[key] = false;
        }
        foreach (var weapon in Weapons)
        {
            if (weapon.tag == tag)
            {
                weapon.gameObject.SetActive(true);
                SaveScript.EquippedWeapon[tag] = true;
            }
            else
            {
                weapon.gameObject.SetActive(false);
            }
        }

        if (tag == "Gun")
        {
            Anim.SetBool("Meele", false);
            AudioPlayer.clip = GunWeaponChange;
            GunUI.gameObject.SetActive(true);
            BulletsUI.gameObject.SetActive(true);
        }
        else
        {
            Anim.SetBool("Meele", true);
            AudioPlayer.clip = MeeleWeaponChange;
            GunUI.gameObject.SetActive(false);
            BulletsUI.gameObject.SetActive(false);
        }

        AudioPlayer.Play();
    }
}
