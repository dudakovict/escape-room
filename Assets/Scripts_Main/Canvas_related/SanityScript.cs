using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SanityScript : MonoBehaviour
{
    [SerializeField] Text SanityText;
    [SerializeField] GameObject DeathPanel;

    void Start()
    {
        DeathPanel.gameObject.SetActive(false);
        SanityText.text = SaveScript.PlayerSanity + "%";
    }

    void Update()
    {
        if (SaveScript.SanityChanged)
        {
            SanityText.text = SaveScript.PlayerSanity + "%";
            SaveScript.SanityChanged = false;
        }

        if (SaveScript.PlayerSanity <= 0)
        {
            SaveScript.PlayerSanity = 0;
            DeathPanel.gameObject.SetActive(true);
        }
    }
}
