using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Crouch : MonoBehaviour
{
    // Update is called once per frame
    void Update()
    {
        if (Input.GetKey(KeyCode.C)) gameObject.transform.localScale = new Vector3(1.1f, 0.7f, 1.1f);
        else gameObject.transform.localScale = new Vector3(1.1f, 1.1f, 1.1f);

    }
}
