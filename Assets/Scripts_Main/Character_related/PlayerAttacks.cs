using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerAttacks : MonoBehaviour
{
    private Animator Anim;
    public float AttackStamina;
    [SerializeField] float MaxAttackStamina = 20.0f;
    [SerializeField] float AttackDrain = 2.0f;
    [SerializeField] float AttackRefill = 1.0f;
    [SerializeField] GameObject Crosshair;
    [SerializeField] GameObject Pointer;
    private AudioSource AudioPlayer;

    // Start is called before the first frame update
    void Start()
    {
        Anim = GetComponent<Animator>();
        AudioPlayer = GetComponent<AudioSource>();
        AttackStamina = MaxAttackStamina;
        Crosshair.gameObject.SetActive(false);
    }

    // Update is called once per frame
    void Update()
    {
        // Currently moving an object
        if (SaveScript.HoldingObjectID != -1) return;
        
        // R unequips weapon
        if (Input.GetKeyDown(KeyCode.R))
        {
            SaveScript.PreviouslyEquippedWeaponTag = "";
            gameObject.SetActive(false);
        }
        
        if (AttackStamina < MaxAttackStamina)
        {
            AttackStamina += AttackRefill * Time.deltaTime;
        }
        if (AttackStamina <= 0.1)
        {
            AttackStamina = 0.1f;
        }
        if (AttackStamina > 3.0)
        {
            if (SaveScript.EquippedWeapon["Knife"])
            {
                if (Input.GetKeyDown(KeyCode.Mouse0))
                {
                    Anim.SetTrigger("KnifeLMB");
                    AttackStamina -= AttackDrain;
                }
                if (Input.GetKeyDown(KeyCode.Mouse1))
                {
                    Anim.SetTrigger("KnifeRMB");
                    AttackStamina -= AttackDrain;
                }
            }

            if (SaveScript.EquippedWeapon["Bat"])
            {
                if (Input.GetKeyDown(KeyCode.Mouse0))
                {
                    Anim.SetTrigger("BatLMB");
                    AttackStamina -= AttackDrain;
                }
                if (Input.GetKeyDown(KeyCode.Mouse1))
                {
                    Anim.SetTrigger("BatRMB");
                    AttackStamina -= AttackDrain;
                }
            }

            if (SaveScript.EquippedWeapon["Machete"])
            {
                if (Input.GetKeyDown(KeyCode.Mouse0))
                {
                    Anim.SetTrigger("KnifeLMB");
                    AttackStamina -= AttackDrain;
                }
                if (Input.GetKeyDown(KeyCode.Mouse1))
                {
                    Anim.SetTrigger("KnifeRMB");
                    AttackStamina -= AttackDrain;
                }
            }

            if (SaveScript.EquippedWeapon["Axe"])
            {
                if (Input.GetKeyDown(KeyCode.Mouse0))
                {
                    Anim.SetTrigger("AxeLMB");
                    AttackStamina -= AttackDrain;
                }
                if (Input.GetKeyDown(KeyCode.Mouse1))
                {
                    Anim.SetTrigger("AxeRMB");
                    AttackStamina -= AttackDrain;
                }
            }

            if (SaveScript.EquippedWeapon["Gun"])
            {
                if (Input.GetKey(KeyCode.Mouse1))
                {
                    Anim.SetBool("AimGun", true);
                    Crosshair.gameObject.SetActive(true);
                    Pointer.gameObject.SetActive(false);
                }
                if (Input.GetKeyUp(KeyCode.Mouse1))
                {
                    Anim.SetBool("AimGun", false);
                    Crosshair.gameObject.SetActive(false);
                    Pointer.gameObject.SetActive(true);
                }
                if (Input.GetKeyDown(KeyCode.Mouse0))
                {
                    if (SaveScript.Bullets > 0)
                    {
                        AudioPlayer.Play();
                    }
                }
            }
        }
    }
}
