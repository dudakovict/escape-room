using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ElectricityExplode : MonoBehaviour
{
    public GameObject Smoke;
    public GameObject Lightning;
    public AudioSource ElectricitySound;
    public AudioSource PowerOffSound;
    public AudioSource ExplodeSound;
    public AudioSource EarRingingSound;
    public AudioSource HeartbeatSound;
    
    public Animator HurtUIAnimator;
    public Animator FPSControllerRotate;

    public ShakeCamera CameraShake;

    public Material blackMaterial;

    public GameObject FPSArms;

    private Outline outlineObj;
    private Camera mainCamera;

    public UIMessageSystem MessageSystem;

    // Start is called before the first frame update
    void Start()
    {
        outlineObj = GetComponent<Outline>();
        mainCamera = Camera.main.GetComponent<Camera>();
    }

    public void HandleElectricityExplode()
    {

        if (!SaveScript.StartingAnimationFinished) return;
        if (SaveScript.ElectricityBoxExploded) return;

        if (gameObject.tag == "Outlineable") outlineObj.OutlineWidth = 10f;

        MessageSystem.DisableAll();
        MessageSystem.EnableSanity();

        if (!SaveScript.PickedUpElectricityKey)
            MessageSystem.EnableElectricityMessage_NeedKey();
        else if (SaveScript.PickedUpElectricityKey && !SaveScript.PickedUpWaterBucket)
            MessageSystem.EnableElectricityMessage_NeedWater();
        else if (SaveScript.PickedUpElectricityKey && SaveScript.PickedUpWaterBucket &&
                !SaveScript.FilledBucketWithWater)
            MessageSystem.EnableElectricityMessage_NeedFillBucket();
        else if (SaveScript.PickedUpElectricityKey && SaveScript.PickedUpWaterBucket &&
                SaveScript.FilledBucketWithWater)
            MessageSystem.EnableElectricityMessage_NeedSplash();


        if (Input.GetKeyDown(KeyCode.E) && SaveScript.ElectricityBoxDoorOpened
            && SaveScript.FilledBucketWithWater)
        {
            FPSArms.SetActive(false);
            SaveScript.ElectricityBoxExploded = true;
            ElectricitySound.Play();
            Lightning.SetActive(true);
            PowerOffSound.Play();
            Smoke.SetActive(true);

            ExplodeSound.Play();
            EarRingingSound.Play();
            HeartbeatSound.Play();

            gameObject.GetComponent<MeshRenderer>().material = blackMaterial;

            FPSControllerRotate.SetBool("Cam_start", true);
            StartCoroutine("AnimateHurt");
            StartCoroutine(CameraShake.Shake(5f, 2f));
        }
    }

    private void OnMouseExit()
    {
        if (Time.deltaTime == 0) return;

        outlineObj.OutlineWidth = 0f;
        MessageSystem.EnableCrosshair();
        MessageSystem.DisableElectricityMessage_NeedKey();
        MessageSystem.DisableElectricityMessage_NeedWater();
        MessageSystem.DisableElectricityMessage_NeedFillBucket();
        MessageSystem.DisableElectricityMessage_NeedSplash();
    }

    public virtual IEnumerator AnimateHurt()
    {
        for(int i=0; i<10; i++)
        {
            HurtUIAnimator.SetTrigger("Hurt");
            yield return new WaitForSeconds(1);
        }
    }
}

