using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WaterBucket : MonoBehaviour
{
    public UIMessageSystem MessageSystem;
    private Outline outlineObj;
    private Camera mainCamera;

    // Start is called before the first frame update
    void Start()
    {
        outlineObj = GetComponent<Outline>();
        mainCamera = Camera.main.GetComponent<Camera>();
    }

    // Update is called once per frame
    void Update()
    {
        if (!SaveScript.StartingAnimationFinished) return;
        if (SaveScript.PickedUpWaterBucket) return;

        Ray ray = mainCamera.ScreenPointToRay(Input.mousePosition);
        RaycastHit hit;

        if (Physics.Raycast(ray, out hit, 3f))
        {
            if (hit.transform.gameObject.GetInstanceID() != gameObject.GetInstanceID()) return;
            if (gameObject.tag == "Outlineable")
            {
                outlineObj.OutlineWidth = 10f;
            }

            MessageSystem.DisableAll();
            MessageSystem.EnableSanity();

            if (!SaveScript.PickedUpWaterBucket)
                MessageSystem.EnableBucketMessage_Pickup();

            if (Input.GetKeyDown(KeyCode.E))
            {
                SaveScript.PickedUpWaterBucket = true;
                Destroy(gameObject);

                MessageSystem.EnableCrosshair();
                MessageSystem.DisableBucketMessage_Pickup();
            }
        }
        else
        {
            MessageSystem.EnableCrosshair();
            MessageSystem.DisableBucketMessage_Pickup();
            if (outlineObj.OutlineWidth != 0f)
                outlineObj.OutlineWidth = 0f;
        }
    }

    private void OnMouseExit()
    {
        MessageSystem.EnableCrosshair();
        MessageSystem.DisableBucketMessage_Pickup();
        outlineObj.OutlineWidth = 0f;
    }
}
