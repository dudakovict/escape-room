using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ChaseMusic : MonoBehaviour
{
    [SerializeField] GameObject ChaseMusicObject;
    void Start()
    {
        StartCoroutine(StartElements());
    }

    void Update()
    {
        if (!SaveScript.ChaseMusicPlaying)
        {
            ChaseMusicObject.gameObject.SetActive(false);
        }

        if (SaveScript.ChaseMusicPlaying)
        {
            ChaseMusicObject.gameObject.SetActive(true);
        }
    }

    IEnumerator StartElements()
    {
        yield return new WaitForSeconds(0.1f);
        ChaseMusicObject = SaveScript.Chase;
        ChaseMusicObject.gameObject.SetActive(false);
    }
}
