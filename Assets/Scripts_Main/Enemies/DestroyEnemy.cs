using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DestroyEnemy : MonoBehaviour
{
    [SerializeField] int EnemyNo;
    void Start()
    {
        if (EnemyNo == 1)
        {
            SaveScript.SkinnyZombie = 0;
        }
        if (EnemyNo == 2)
        {
            SaveScript.FastZombie = 0;
        }
        if (EnemyNo == 3)
        {
            SaveScript.FatZombie = 0;
        }
    }
}
