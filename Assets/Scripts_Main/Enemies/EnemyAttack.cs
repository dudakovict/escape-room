using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class EnemyAttack : MonoBehaviour
{
    private NavMeshAgent Nav;
    private NavMeshHit hit;
    private bool Blocked = false;
    private bool RunToPlayer = false;
    private float DistanceToPlayer;
    private bool IsChecking = true;
    private bool CanRun = false;
    private int MadnessCounter = 0;
    private int FailedChecks = 0;
    private AudioSource AudioPlayer;
    [SerializeField] Transform Player;
    [SerializeField] Animator Anim;
    [SerializeField] GameObject Enemy;
    [SerializeField] GameObject HurtUI;
    [SerializeField] float MaxRange = 10.0f;
    [SerializeField] int MaxChecks = 3;
    [SerializeField] float ChaseSpeed = 8.5f;
    [SerializeField] float WalkSpeed = 0.5f;
    [SerializeField] float AttackDistance = 2.3f;
    [SerializeField] float AttackRotateSpeed = 2.0f;
    [SerializeField] float CheckTime = 3.0f;
    [SerializeField] GameObject EnemyDamageZone;
    [SerializeField] bool Knife;
    [SerializeField] bool Machete;
    [SerializeField] bool Axe;
    [SerializeField] bool Bat;

    void Start()
    {
        Nav = GetComponentInParent<NavMeshAgent>();
        StartCoroutine(StartEelements());
        AudioPlayer = GetComponent<AudioSource>();
    }

    void Update()
    {
        if (CanRun)
        {
            if (EnemyDamageZone.GetComponent<EnemyDamage>().HasDied)
            {
                SaveScript.ChaseMusicPlaying = false;
            }
            DistanceToPlayer = Vector3.Distance(Player.position, Enemy.transform.position);
            if (DistanceToPlayer < MaxRange)
            {
                if (IsChecking)
                {
                    IsChecking = false;
                    Blocked = NavMesh.Raycast(transform.position, Player.position, out hit, NavMesh.AllAreas);

                    if (!Blocked)
                    {
                        MadnessCounter++;
                        if (MadnessCounter == 1)
                        {
                            AudioPlayer.Play();
                        }
                        RunToPlayer = true;
                        FailedChecks = 0;
                    }
                    if (Blocked)
                    {
                        RunToPlayer = false;
                        Anim.SetInteger("State", 1);
                        FailedChecks++;
                    }

                    StartCoroutine(TimedCheck());
                }
            }

            if (RunToPlayer)
            {
                Enemy.GetComponent<EnemyMove>().enabled = false;
                if (!EnemyDamageZone.GetComponent<EnemyDamage>().HasDied)
                {
                    SaveScript.GotScared = true;
                    SaveScript.ChaseMusicPlaying = true;
                }
                if (DistanceToPlayer > AttackDistance)
                {
                    Nav.isStopped = false;
                    Anim.SetInteger("State", 2);
                    Nav.acceleration = 24;
                    Nav.SetDestination(Player.position);
                    Nav.speed = ChaseSpeed;
                    HurtUI.gameObject.SetActive(false);
                }
                if (DistanceToPlayer < AttackDistance - 0.5f)
                {
                    Nav.isStopped = true;
                    Nav.acceleration = 180;
                    if (Machete)
                    {
                        Anim.SetInteger("State", 3);
                    }
                    if (Knife)
                    {
                        Anim.SetInteger("State", 4);
                    }
                    if (Axe || Bat)
                    {
                        Anim.SetInteger("State", 5);
                    }
                    HurtUI.gameObject.SetActive(true);

                    Vector3 Pos = (Player.position - Enemy.transform.position).normalized;
                    Quaternion PosRotation = Quaternion.LookRotation(new Vector3(Pos.x, 0, Pos.z));

                    Enemy.transform.rotation = Quaternion.Slerp(Enemy.transform.rotation, PosRotation, Time.deltaTime * AttackRotateSpeed);
                }
            }
            else if (!RunToPlayer)
            {
                MadnessCounter = 0;
                Nav.isStopped = true;
                SaveScript.ChaseMusicPlaying = false;
            }
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Player"))
        {
            RunToPlayer = true;
        }
        if (other.gameObject.CompareTag("Knife"))
        {
            Anim.SetTrigger("SmallReact");
        }
        if (other.gameObject.CompareTag("Bat"))
        {
            Anim.SetTrigger("SmallReact");
        }
        if (other.gameObject.CompareTag("Axe"))
        {
            Anim.SetTrigger("BigReact");
        }
        if (other.gameObject.CompareTag("Machete"))
        {
            Anim.SetTrigger("BigReact");
        }
    }

    IEnumerator TimedCheck()
    {
        yield return new WaitForSeconds(CheckTime);
        IsChecking = true;

        if (FailedChecks > MaxChecks)
        {
            Enemy.GetComponent<EnemyMove>().enabled = true;
            Nav.isStopped = false;
            Nav.speed = WalkSpeed;
            FailedChecks = 0;
            SaveScript.ChaseMusicPlaying = false;
        }
    }

    IEnumerator StartEelements()
    {
        yield return new WaitForSeconds(0.1f);
        Player = SaveScript.PlayerCharacter;
        HurtUI = SaveScript.Hurt;
        CanRun = true;
        CheckTime = Random.Range(3, 15);
    }
}
