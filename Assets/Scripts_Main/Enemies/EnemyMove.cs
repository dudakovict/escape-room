using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class EnemyMove : MonoBehaviour
{
    private NavMeshAgent Nav;
    private Animator Anim;
    private Transform Target;
    private float DistanceToTarget;
    private int TargetNumber = 1;
    private bool HasStopped = false;
    private bool Randomizer = true;
    private int NextTargetNumber;
    private AudioSource AudioPlayer;
    private bool CanPatrol = false;
    [SerializeField] float StopDistance = 2.0f;
    [SerializeField] float WaitTime = 2.0f;
    [SerializeField] Transform Target1;
    [SerializeField] Transform Target2;
    [SerializeField] Transform Target3;
    [SerializeField] int MaxTargets = 3;
    
    void Start()
    {
        Nav = GetComponent<NavMeshAgent>();
        Anim = GetComponent<Animator>();
        AudioPlayer = GetComponent<AudioSource>();
        StartCoroutine(StartEelements());
    }

    void Update()
    {
        if (CanPatrol)
        {
            DistanceToTarget = Vector3.Distance(Target.position, transform.position);
            if (DistanceToTarget > StopDistance)
            {
                Nav.SetDestination(Target.position);
                Anim.SetInteger("State", 0);
                Nav.isStopped = false;
                NextTargetNumber = TargetNumber;
            }
            if (DistanceToTarget < StopDistance)
            {
                Nav.isStopped = true;
                Anim.SetInteger("State", 1);
                StartCoroutine(LookAround());
            }
        }
    }

    private void SetTarget()
    {
        if (TargetNumber == 1)
        {
            Target = Target1;
        }
        if (TargetNumber == 2)
        {
            Target = Target2;
        }
        if (TargetNumber == 3)
        {
            Target = Target3;
        }
    }

    IEnumerator LookAround()
    {
        yield return new WaitForSeconds(WaitTime);
        AudioPlayer.Play();
        if (!HasStopped)
        {
            HasStopped = true;
            if (Randomizer)
            {
                Randomizer = false;
                TargetNumber = Random.Range(1, MaxTargets + 1);
                if (TargetNumber == NextTargetNumber)
                {
                    TargetNumber++;
                    if (TargetNumber >= MaxTargets)
                    {
                        TargetNumber = 1;
                    }
                }
            }
            SetTarget();
            yield return new WaitForSeconds(WaitTime);
            HasStopped = false;
            Randomizer = true;
        }
    }

    IEnumerator StartEelements()
    {
        yield return new WaitForSeconds(0.1f);
        Target1 = SaveScript.Target1;
        Target2 = SaveScript.Target2;
        Target3 = SaveScript.Target3;
        Target = Target1;
        Nav.avoidancePriority = Random.Range(5, 65);
        CanPatrol = true;
    }
}
