using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyWeaponDamage : MonoBehaviour
{
    [SerializeField] int WeaponDamage = 1;
    [SerializeField] Animator HurtAnim;
    [SerializeField] AudioSource AudioPlayer;
    private bool HitActive = false;
    [SerializeField] GameObject FPSArms;
    [SerializeField] AudioClip DeathScream;

    private void Start()
    {
        StartCoroutine(StartElements()); 
    }

    private void Update()
    {
        if (SaveScript.DifficultyChanged)
        {
            WeaponDamage = SaveScript.EnemyWeaponDamage;
            SaveScript.DifficultyChanged = false;
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Player"))
        {
            if (!HitActive)
            {
                HitActive = true;
                HurtAnim.SetTrigger("Hurt");
                SaveScript.PlayerSanity -= WeaponDamage;
                if (SaveScript.PlayerSanity <= 0)
                {
                    AudioPlayer.clip = DeathScream;
                }
                SaveScript.SanityChanged = true;
                AudioPlayer.Play();
                FPSArms.GetComponent<PlayerAttacks>().AttackStamina -= 1;
            }
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.gameObject.CompareTag("Player"))
        {
            if (HitActive)
            {
                HitActive = false;
            }
        }
    }

    IEnumerator StartElements()
    {
        yield return new WaitForSeconds(0.1f);
        HurtAnim = SaveScript.HurtAnimator;
        AudioPlayer = SaveScript.AudioEWD;
        FPSArms = SaveScript.Arms;
        WeaponDamage = SaveScript.EnemyWeaponDamage;
        DeathScream = SaveScript.DS;
    }
}
