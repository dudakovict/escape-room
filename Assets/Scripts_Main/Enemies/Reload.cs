using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Reload : MonoBehaviour
{
    [SerializeField] GameObject SkinnyZombie;
    [SerializeField] GameObject FastZombie;
    [SerializeField] GameObject FatZombie;
    void Start()
    {
        StartCoroutine(WaitToDestroy());
    }

    void Update()
    {
        
    }

    IEnumerator WaitToDestroy()
    {
        yield return new WaitForSeconds(1);
        if (SaveScript.SkinnyZombie == 0)
        {
            Destroy(SkinnyZombie.gameObject);
        }
        if (SaveScript.FastZombie == 0)
        {
            Destroy(FastZombie.gameObject);
        }
        if (SaveScript.FatZombie == 0)
        {
            Destroy(FatZombie.gameObject);
        }
        if (SaveScript.EnemiesOnScreen == 2)
        {
            FastZombie.gameObject.SetActive(true);
            FatZombie.gameObject.SetActive(false);
        }
        if (SaveScript.EnemiesOnScreen == 1)
        {
            FatZombie.gameObject.SetActive(true);
        }
        if (SaveScript.EnemiesOnScreen == 0)
        {
            FatZombie.gameObject.SetActive(false);
        }
    }
}
