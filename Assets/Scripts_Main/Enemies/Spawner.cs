using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Spawner : MonoBehaviour
{
    [SerializeField] GameObject EnemySpawn;
    [SerializeField] Transform SpawnPoint;
    [SerializeField] bool Retriggerable;
    private bool CanSpawn = true;
    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Player"))
        {
            if (SaveScript.EnemiesOnScreen < SaveScript.MaxEnemiesOnScreen)
            {
                if (CanSpawn)
                {
                    CanSpawn = false;
                    Instantiate(EnemySpawn, SpawnPoint.position, SpawnPoint.rotation);
                    SaveScript.EnemiesOnScreen++;
                    if (Retriggerable)
                    {
                        StartCoroutine(WaitToSpawn());
                    }
                }
            }
        }
    }

    IEnumerator WaitToSpawn()
    {
        yield return new WaitForSeconds(2f);
        CanSpawn = true;
    }
}
