using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Drag : MonoBehaviour
{
    private Vector3 lastPos, currPos;

    private Vector3 firstPosition;
    private Quaternion firstRotation;

    void Start()
    {
        lastPos = Input.mousePosition;
        firstPosition = transform.position;
        firstRotation = transform.rotation;
    }

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.R))
        {
            transform.SetPositionAndRotation(firstPosition, firstRotation);
        }

        if (Input.GetMouseButton(0))
        {
            currPos = Input.mousePosition;
            
            var delta = Input.mousePosition - lastPos;
            var axis = Quaternion.AngleAxis(-90f, Vector3.forward) * delta;
            transform.rotation = Quaternion.AngleAxis(delta.magnitude * 0.3f, axis) * transform.rotation;
        }
        lastPos = Input.mousePosition;
    }
}
