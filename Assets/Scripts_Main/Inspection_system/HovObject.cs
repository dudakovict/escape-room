using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HovObject : MonoBehaviour
{
    public UIMessageSystem MessageSystem;
    public GameObject Inspection;
    public InspectionObj inspectionObj;
    public GameObject FPSController;
    public GameObject InspectMessage;
    public GameObject FPSArms;
    public int index;

    public Vector3 SavedRotation;
    private Camera _mainCamera;
    private Outline _outlineObj;

    private void Start()
    {
        _mainCamera = Camera.main.GetComponent<Camera>();
        _outlineObj = gameObject.GetComponent<Outline>();
    }

    public void HandleInspect()
    {
        if (!SaveScript.StartingAnimationFinished) return;

        // Currently moving an object
        if (SaveScript.HoldingObjectID != -1) return;

        if (Time.timeScale == 0) return;

        if (SaveScript.InInventoryMode || SaveScript.InOptionsMode) return;

        if (gameObject.tag == "Outlineable")
        {
            _outlineObj.OutlineWidth = 10f;
            MessageSystem.DisableAll();
            MessageSystem.EnableSanity();
            MessageSystem.EnableInspectMessage();
        }
        if (Input.GetKeyDown(KeyCode.V))
        {
            SaveScript.InInspectMode = true;

            FPSArms.SetActive(false);
            SaveScript.PreviouslyEquippedWeaponTag = "";

            MessageSystem.DisableAll();
            MessageSystem.EnableSanity();

            Inspection.SetActive(true);
            inspectionObj.TurnOnInspection(index);
            FPSController.GetComponent<UnityStandardAssets.Characters.FirstPerson.FirstPersonController>().enabled = false;
            Time.timeScale = 0;
            Cursor.lockState = CursorLockMode.Confined;
            Cursor.visible = true;
        }
    }

    public void DisableOutline()
    {
        SaveScript.InInspectMode = false;
        _outlineObj.OutlineWidth = 0f;
    }

    private void OnMouseExit()
    {
        // if currently looking at object don't change anything behind it
        if (Time.timeScale == 0) return;

        SaveScript.InInspectMode = false;
        _outlineObj.OutlineWidth = 0f;
        MessageSystem.DisableInspectMessage();
        MessageSystem.EnableSanity();
        MessageSystem.EnableCrosshair();
    }
}
