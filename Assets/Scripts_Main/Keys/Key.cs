public class Key
{
    private readonly string Tag;
    private bool Active;

    public Key(string tag)
    {
        Tag = tag;
        Active = false;
    }

    public string GetTag()
    {
        return Tag;
    }

    public bool GetActive()
    {
        return Active;
    }

    public void SetActive(bool active)
    {
        Active = active;
    }

    public static int GetKeyIndex(Key[] keys, string tag)
    {
        for (var i = 0; i < keys.Length; i++)
        {
            if (keys[i].GetTag() == tag)
            {
                return i;
            }
        }
        return -1;
    }
}
