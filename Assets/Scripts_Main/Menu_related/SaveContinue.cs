using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SaveContinue : MonoBehaviour
{
    [SerializeField] GameObject Continue;
    public int DataExists;
    void Start()
    {
        if (Continue != null)
        {
            Continue.gameObject.SetActive(false);
            DataExists = PlayerPrefs.GetInt("PlayersSanity", 0);
            if (DataExists > 0)
            {
                Continue.gameObject.SetActive(true);
            }
        }
    }

    public void LoadGameData()
    {
        SaveScript.SavedGame = true;

    }
    public void SaveGame()
    {
        if (SaveScript.InOptionsMode)
        {
            PlayerPrefs.SetInt("InOptionsMenuSaved", 1);
        }
        if (SaveScript.OpenedBedroomDoors)
        {
            PlayerPrefs.SetInt("OpenedBedroomDoors", 1);
        }
        if (SaveScript.ElectricityBoxDoorOpened)
        {
            PlayerPrefs.SetInt("ElectricityBoxDoorOpened", 1);
        }
        PlayerPrefs.SetInt("PlayersSanity", SaveScript.PlayerSanity);
        PlayerPrefs.SetFloat("BatteriesPower", SaveScript.BatteryPower);
        PlayerPrefs.SetInt("ApplesCount", SaveScript.Apples);
        PlayerPrefs.SetInt("BatteriesCount", SaveScript.Batteries);
        PlayerPrefs.SetInt("ApplesLeftIG", SaveScript.ApplesLeft);
        PlayerPrefs.SetInt("BatteriesLeftIG", SaveScript.BatteriesLeft);
        PlayerPrefs.SetInt("AmmoLeftIG", SaveScript.AmmoLeft);
        PlayerPrefs.SetInt("SkinnyZombieAlive", SaveScript.SkinnyZombie);
        PlayerPrefs.SetInt("FastZombieAlive", SaveScript.FastZombie);
        PlayerPrefs.SetInt("FatZombieAlive", SaveScript.FatZombie);
        PlayerPrefs.SetInt("EnemiesOnScreenSaved", SaveScript.EnemiesOnScreen);
        PlayerPrefs.SetInt("OneshotTriggeredSaved", SaveScript.OneshotTriggered);
        PlayerPrefs.SetFloat("PlayerPositionX", SaveScript.PlayerCharacter.transform.position.x);
        PlayerPrefs.SetFloat("PlayerPositionY", SaveScript.PlayerCharacter.transform.position.y);
        PlayerPrefs.SetFloat("PlayerPositionZ", SaveScript.PlayerCharacter.transform.position.z);
        for (var i = 0; i < SaveScript.Weapons.Length; i++)
        {
            if (SaveScript.Weapons[i].GetActive())
            {
                PlayerPrefs.SetInt("Have" + SaveScript.Weapons[i].GetTag(), 1);
            }
        }
        for (var i = 0; i < SaveScript.Keys.Length; i++)
        {
            if (SaveScript.Keys[i].GetActive())
            {
                PlayerPrefs.SetInt("Have" + SaveScript.Keys[i].GetTag(), 1);
            }
        }
        if (SaveScript.FlashLight)
        {
            PlayerPrefs.SetInt("FL", 1);
        }
        if (SaveScript.NVGoggles)
        {
            PlayerPrefs.SetInt("NV", 1);
        }
        if (SaveScript.StartingAnimationFinished)
        {
            PlayerPrefs.SetInt("StartingAnimFinished", 1);
        }
        if (SaveScript.PickedUpLighter)
        {
            PlayerPrefs.SetInt("PickedUpLighterSaved", 1);
        }
        if (SaveScript.ElectricityBoxDoorOpened)
        {
            PlayerPrefs.SetInt("ElectricityBoxDoorOpenedSaved", 1);
        }
        if (SaveScript.PickedUpElectricityKey)
        {
            PlayerPrefs.SetInt("PickedUpElectricityKeySaved", 1);
        }
        if (SaveScript.PickedUpWaterBucket)
        {
            PlayerPrefs.SetInt("PickedUpWaterBucketSaved", 1);
        }
        if (SaveScript.FilledBucketWithWater)
        {
            PlayerPrefs.SetInt("FilledBucketWithWaterSaved", 1);
        }
        if (SaveScript.ElectricityBoxExploded)
        {
            PlayerPrefs.SetInt("ElectricityBoxExplodedSaved", 1);
        }
        if (SaveScript.OfficeDoorDestroyed)
        {
            PlayerPrefs.SetInt("OfficeDoorDestroyedSaved", 1);
        }
    }
}
