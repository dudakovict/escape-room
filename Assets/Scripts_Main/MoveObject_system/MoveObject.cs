using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoveObject : MonoBehaviour
{
    public UIMessageSystem MessageSystem;
    public GameObject tempParent;
    public GameObject MoveMessage, ThrowMessage, CenterKnob;
    public GameObject FPSArms;

    private Outline outlineObj;
    private Rigidbody rb;

    bool isHolding = false;
    bool isThrown = false;
    
    float throwForce = 600;
    Vector3 objectPos;
    float distance;

    private void Start()
    {
        outlineObj = gameObject.GetComponent<Outline>();
        rb = gameObject.GetComponent<Rigidbody>();
    }

    void Update()
    {
        if (SaveScript.InInspectMode || 
            SaveScript.InInventoryMode || 
            SaveScript.InOptionsMode) return;

        if (SaveScript.HoldingObjectID != gameObject.GetInstanceID() &&
            SaveScript.HoldingObjectID != -1) return;

        distance = Vector3.Distance(transform.position, tempParent.transform.position);
        if (distance >= 2f)
        {
            isHolding = false;
            SaveScript.HoldingObjectID = -1;
        }

        //Check if is holding
        if (isHolding)
        {
            FPSArms.SetActive(false);
            SaveScript.PreviouslyEquippedWeaponTag = "";
            
            rb.velocity = Vector3.zero;
            rb.angularVelocity = Vector3.zero;
            transform.SetParent(tempParent.transform);

            if (Input.GetKeyDown(KeyCode.Q))
            {
                // throw
                rb.AddForce(tempParent.transform.forward * throwForce);
                SaveScript.HoldingObjectID = -1;
                isHolding = false;
                isThrown = true;
            }
        }
        else
        {
            objectPos = transform.position;
            transform.SetParent(null);
            rb.useGravity = true;
            transform.position = objectPos;
            SaveScript.HoldingObjectID = -1;
        }
    }

    private void OnMouseOver()
    {
        if (SaveScript.HoldingObjectID != gameObject.GetInstanceID() &&
            SaveScript.HoldingObjectID != -1) return;

        if (!isHolding && distance <= 2f)
        {
            outlineObj.OutlineColor = new Color(255, 255, 0);
            outlineObj.OutlineWidth = 10f;

            MessageSystem.DisableAll();
            MessageSystem.EnableMoveMessage();
            MessageSystem.EnableSanity();
        }

        if (Input.GetMouseButton(1) && distance <= 2f && !isThrown)
        {
            outlineObj.OutlineColor = new Color(255, 0, 0);
            outlineObj.OutlineWidth = 10f;

            MessageSystem.DisableAll();
            MessageSystem.EnableThrowMessage();
            MessageSystem.EnableSanity();

            SaveScript.HoldingObjectID = gameObject.GetInstanceID();
            isHolding = true;
            rb.useGravity = false;
            rb.detectCollisions = true;
        }

        if (Input.GetMouseButtonUp(1) || distance > 2f || (!isHolding && distance > 2f))
        {
            isHolding = false;
            isThrown = false;
            SaveScript.HoldingObjectID = -1;

            outlineObj.OutlineWidth = 0f;

            MessageSystem.DisableMoveMessage();
            MessageSystem.DisableThrowMessage();
            MessageSystem.EnableCrosshair();
            MessageSystem.EnableSanity();

            objectPos = transform.position;
            transform.SetParent(null);
            rb.useGravity = true;
            transform.position = objectPos;
        }
    }

    private void OnMouseExit()
    {
        outlineObj.OutlineWidth = 0f;
        
        objectPos = transform.position;
        transform.SetParent(null);
        rb.useGravity = true;
        transform.position = objectPos;

        MessageSystem.DisableMoveMessage();
        MessageSystem.DisableThrowMessage();
        MessageSystem.EnableCrosshair();
        MessageSystem.EnableSanity();
        
        isHolding = false;
        isThrown = false;
        SaveScript.HoldingObjectID = -1;
    }
}
