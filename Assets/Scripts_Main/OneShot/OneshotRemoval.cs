using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OneshotRemoval : MonoBehaviour
{
    [SerializeField] GameObject[] Oneshots;
    void Start()
    {
        StartCoroutine(RemoveOneshots());
    }

    IEnumerator RemoveOneshots()
    {
        yield return new WaitForSeconds(0.1f);
        for (var i = 0; i < SaveScript.OneshotTriggered; i++)
        {
            Debug.Log(i);
            Destroy(Oneshots[i].gameObject);
        }
    }
}
