using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BatteryPickup : MonoBehaviour
{
    [SerializeField] int BatteriesCount;
    void Start()
    {
        StartCoroutine(CheckBatteries());
    }

    IEnumerator CheckBatteries()
    {
        yield return new WaitForSeconds(1);
        if (BatteriesCount > SaveScript.BatteriesLeft)
        {
            Destroy(gameObject);
        }
    }
}
