using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;

public class Pickups : MonoBehaviour
{
    RaycastHit hit;
    [SerializeField] GameObject PickupMessage;
    [SerializeField] GameObject PlayerArms;
    [SerializeField] GameObject DoorMessage;
    [SerializeField] Text DoorText;
    [SerializeField] GameObject NVMessage;
    [SerializeField] GameObject FLMessage;

    public UIMessageSystem MessageSystem;
    private Camera _mainCamera;
    private AudioSource AudioPlayer;
    private string _lastHitType = "";

    /************************************************************************************************
                                        FULL LIST OF UNITY TAGS
     ************************************************************************************************/
    private readonly string[] WEAPON_TAGS = new string[5] { "Knife", "Bat", "Machete", "Axe", "Gun" };
    private readonly string[] UTILITY_TAGS = new string[7] { "Lighter", "Apple", "Battery", "Ammo", "Water", "NVGoggles", "Flashlight"};
    private readonly string[] KEY_TAGS = new string[4] { "KitchenMid Key", "Electricity Key", "Basement Key", "GuardRoom Key"};
    private readonly string[] TAGS = new string[17] { "Lighter", "Knife", "Bat", "Machete", "Axe", "Gun", "Apple", "Battery", "Ammo", "WaterBottle", "NVGoggles", "Flashlight", "Door", "KitchenMid Key", "Electricity Key", "Basement Key", "GuardRoom Key" };

    void Start()
    {
        _mainCamera = Camera.main.GetComponent<Camera>();
        HandleNVMessage(false);
        HandleFLMessage(false);
        PlayerArms.gameObject.SetActive(false);
        AudioPlayer = Camera.main.GetComponent<AudioSource>();
    }


    /************************************************************************************************
                                        MESSAGE HANDLER FUNCTIONS
     ************************************************************************************************/
    private void HandlePickupMessage(bool status)
    {
        MessageSystem.DisableAll();
        MessageSystem.EnableSanity();
        MessageSystem.EnableCrosshair();
        if (status) MessageSystem.EnablePickupMessage();
    }

    private void HandleDoorMessage(bool status, RaycastHit hit)
    {
        MessageSystem.DisableAll();
        MessageSystem.EnableCrosshair();
        MessageSystem.EnableSanity();
        if (!status) return;

        if (hit.transform.gameObject.GetComponent<DoorScript>().IsLocked)
            MessageSystem.EnableLockedDoorMessage();
        else if (!hit.transform.gameObject.GetComponent<DoorScript>().IsOpen)
            MessageSystem.EnableOpenDoorMessage();
        else if (hit.transform.gameObject.GetComponent<DoorScript>().IsOpen)
            MessageSystem.EnableCloseDoorMessage();
    }

    private void HandleNVMessage(bool status)
    {
        NVMessage.gameObject.SetActive(status);
    }

    private void HandleFLMessage(bool status)
    {
        FLMessage.gameObject.SetActive(status);
    }

    private void HandleMessage(string tag, RaycastHit hit, bool canSeePickup)
    {
        if (canSeePickup)
        {
            if (tag == "Door") HandleDoorMessage(true, hit);
            else HandlePickupMessage(true);
        }
        else
        {
            HandleDoorMessage(false, hit);
            HandlePickupMessage(false);
        }
    }

    /************************************************************************************************
                                        RESOURCE HANDLER FUNCTIONS
     ************************************************************************************************/
    private void HandleWeaponPickup(string tag)
    {
        int idx = Weapon.GetWeaponIndex(SaveScript.Weapons, tag);
        if (idx != -1)
        {
            Destroy(hit.transform.gameObject);
            SaveScript.Weapons[idx].SetActive(true);
            AudioPlayer.Play();

            MessageSystem.DisablePickupMessage();
        }
    }

    private void HandleUtilityPickup(string tag)
    {
        if(tag == "Lighter")
        {
            MessageSystem.DisablePickupMessage();

            Destroy(hit.transform.gameObject);
            SaveScript.PickedUpLighter = true;
            AudioPlayer.Play();
        }

        if (tag == "Apple")
        {
            if (SaveScript.Apples < 6)
            {
                Destroy(hit.transform.gameObject);
                SaveScript.ApplesLeft--;
                SaveScript.Apples++;
                AudioPlayer.Play();

                MessageSystem.DisablePickupMessage();
            }
        }
        else if (tag == "Battery")
        {
            if (SaveScript.Batteries < 4)
            {
                Destroy(hit.transform.gameObject);
                SaveScript.BatteriesLeft--;
                SaveScript.Batteries++;
                AudioPlayer.Play();

                MessageSystem.DisablePickupMessage();
            }
        }
        else if (tag == "WaterBottle")
        {
            if (SaveScript.WaterBottles < 6)
            {
                Destroy(hit.transform.gameObject);
                SaveScript.WaterBottles++;
                AudioPlayer.Play();

                MessageSystem.DisablePickupMessage();
            }
        }
        else if (tag == "Ammo")
        {
            if (SaveScript.BulletClips < 4)
            {
                Destroy(hit.transform.gameObject);
                SaveScript.AmmoLeft--;
                SaveScript.BulletClips++;
                AudioPlayer.Play();

                MessageSystem.DisablePickupMessage();
            }
        }
        else if (tag == "NVGoggles")
        {
            if (!SaveScript.NVGoggles)
            {
                Destroy(hit.transform.gameObject);
                SaveScript.NVGoggles = true;
                AudioPlayer.Play();

                MessageSystem.EnableNightVisionMessage();

                MessageSystem.DisablePickupMessage();
            }
        }
        else if (tag == "Flashlight")
        {
            if (!SaveScript.FlashLight)
            {
                Destroy(hit.transform.gameObject);
                SaveScript.FlashLight = true;
                AudioPlayer.Play();

                MessageSystem.EnableFlashlightMessage();

                MessageSystem.DisablePickupMessage();
            }
        }
    }

    private void HandleKeyPickup(string tag)
    {
        int idx = Key.GetKeyIndex(SaveScript.Keys, tag);
        if (idx != -1)
        {
            Destroy(hit.transform.gameObject);
            SaveScript.Keys[idx].SetActive(true);
            AudioPlayer.Play();

            MessageSystem.DisablePickupMessage();
        }
    }

    public void HandlePickup(RaycastHit hit_)
    {
        // Starting animation hasn't finished
        if (!SaveScript.StartingAnimationFinished) return;

        // Currently moving an object
        if (SaveScript.HoldingObjectID != -1) return;

        // Currently inspecting an object or in keypad mode
        if (SaveScript.InInspectMode || SaveScript.InKeypadMode) return;

        // Currently in inventory or options mode
        if (SaveScript.InInventoryMode || SaveScript.InOptionsMode) return;

        hit = hit_;
        string tag = hit.transform.tag;
        if (TAGS.Contains(tag))
        {
            loadLastHitType(tag);
            HandleMessage(tag, hit, true);

            if (Input.GetKeyDown(KeyCode.E))
            {
                if (WEAPON_TAGS.Contains(tag)) HandleWeaponPickup(tag);
                else if (UTILITY_TAGS.Contains(tag)) HandleUtilityPickup(tag);
                else if (KEY_TAGS.Contains(tag)) HandleKeyPickup(tag);
                else if (!hit.transform.gameObject.GetComponent<DoorScript>().IsLocked)
                    hit.transform.gameObject.SendMessage("DoorOpen");
            }
        }
        else HandleMessage(tag, hit, false);
    }

    private void OnMouseExit()
    {
        if (Time.deltaTime == 0) return;

        if (_lastHitType.Equals("Pickup")) MessageSystem.DisablePickupMessage();
        else if (_lastHitType.Equals("Door")) MessageSystem.DisableDoorMessage();

        MessageSystem.DisableAll();
        MessageSystem.EnableSanity();
        MessageSystem.EnableCrosshair();
    }

    private void loadLastHitType(string tag)
    {
        if (WEAPON_TAGS.Contains(tag) || UTILITY_TAGS.Contains(tag) ||
            KEY_TAGS.Contains(tag)) _lastHitType = "Pickup";
        else _lastHitType = "Door";
    }
}
