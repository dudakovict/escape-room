using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SaveScript : MonoBehaviour
{
    public static bool StartingAnimationFinished = false;
    public static int PlayerSanity = 3; // Start animation with 3 sanity
    public static bool SanityChanged = false;
    public static float BatteryPower = 1.0f;
    public static bool BatteryRefill = false;
    public static bool FlashLightOn = false;
    public static bool FlashLight = false;
    public static bool NVLightOn = false;
    public static bool NVGoggles = false;
    public static int Apples = 0;
    public static int Batteries = 0;
    public static int WaterBottles = 0;
    public static bool NewGame = false;
    public static bool SavedGame = false;
    public static int ApplesLeft = 6;
    public static int AmmoLeft = 4;
    public static int BatteriesLeft = 4;
    public static int Bullets = 6;
    public static int BulletClips = 0;
    
    public static int HoldingObjectID = -1;
    public static bool InInventoryMode = false;
    public static bool InOptionsMode = false;
    public static bool InInspectMode = false;
    public static bool InKeypadMode = false;

    public static bool PickedUpLighter = false;
    public static bool OpenedBedroomDoors = false;
    public static bool ElectricityBoxDoorOpened = false;
    public static bool PickedUpElectricityKey = false;
    public static bool PickedUpWaterBucket = false;
    public static bool FilledBucketWithWater = false;
    public static bool ElectricityBoxExploded = false;
    public static bool GasCylinderExploded = false;
    public static bool OfficeDoorDestroyed = false;

    public static bool StartedNewGame = false;
    public static bool GotScared = false;

    public static Transform Target1;
    public static Transform Target2;
    public static Transform Target3;
    public static Transform PlayerCharacter;
    public static GameObject Chase;
    public static GameObject Hurt;
    public static GameObject Blood;
    public static AudioSource Stab;
    public static Animator HurtAnimator;
    public static AudioSource AudioEWD;
    public static GameObject Arms;
    public static int MaxEnemiesOnScreen = 3;
    public static int EnemiesOnScreen = 3;
    public static int FatZombie = 1;
    public static int SkinnyZombie = 1;
    public static int FastZombie = 1;
    public static int EnemyWeaponDamage = 30;
    public static bool DifficultyChanged = false;
    public static bool ChaseMusicPlaying = false;
    public static int OneshotTriggered = 0;

    public static bool SecretTunnelDestroyed = false;
    public static string PreviouslyEquippedWeaponTag = "";
    public static AudioClip DS;

    [SerializeField] Transform _Target1;
    [SerializeField] Transform _Target2;
    [SerializeField] Transform _Target3;
    [SerializeField] Transform PlayerPrefab;
    [SerializeField] GameObject ChaseMusic;
    [SerializeField] GameObject HurtUI;
    [SerializeField] AudioSource StabPlayer;
    [SerializeField] GameObject BloodSplat;
    [SerializeField] Animator HurtAMR;
    [SerializeField] AudioSource EWD;
    [SerializeField] AudioClip DeathSound;
    [SerializeField] GameObject FPSArms;
    public static Dictionary<string, bool> EquippedWeapon = new Dictionary<string, bool>
    {
        {"Knife", false},
        {"Bat", false},
        {"Machete", false},
        {"Axe", false},
        {"Gun", false}
    };

    public static Key[] Keys = new Key[4]
    {
        new Key("KitchenMid Key"),
        new Key("Electricity Key"),
        new Key("Basement Key"),
        new Key("GuardRoom Key")
    };
    public static Weapon[] Weapons = new Weapon[5]
    {
        new Weapon("Knife", 20),
        new Weapon("Bat", 30),
        new Weapon("Machete", 40),
        new Weapon("Axe", 50),
        new Weapon("Gun", 60)
    };

    private void Start()
    {
        Cursor.visible = true;
        Cursor.lockState = CursorLockMode.Locked;
        Target1 = _Target1;
        Target2 = _Target2;
        Target3 = _Target3;
        PlayerCharacter = PlayerPrefab;
        Chase = ChaseMusic;
        Hurt = HurtUI;
        Stab = StabPlayer;
        Blood = BloodSplat;
        HurtAnimator = HurtAMR;
        AudioEWD = EWD;
        DS = DeathSound;
        Arms = FPSArms;
        if (NewGame)
        {
            StartedNewGame = true;
            OneshotTriggered = 0;
            PlayerSanity = 3;
            SanityChanged = true;
            BatteryPower = 1.0f;
            BatteryRefill = false;
            FlashLightOn = false;
            FlashLight = false;
            NVLightOn = false;
            NVGoggles = false;
            OpenedBedroomDoors = false;
            Apples = 0;
            Batteries = 0;
            EquippedWeapon = new Dictionary<string, bool>
            {
                {"Knife", false},
                {"Bat", false},
                {"Machete", false},
                {"Axe", false},
                {"Gun", false}
            };
            ApplesLeft = 6;
            BatteriesLeft = 4;
            AmmoLeft = 4;
            FatZombie = 1;
            SkinnyZombie = 1;
            FastZombie = 1;
            EnemyWeaponDamage = 30;
            NewGame = false;
        }

        if (SavedGame)
        {
            StartedNewGame = false;
            OneshotTriggered = PlayerPrefs.GetInt("OneshotTriggeredSaved");
            EnemiesOnScreen = PlayerPrefs.GetInt("EnemiesOnScreenSaved");
            PlayerSanity = PlayerPrefs.GetInt("PlayersSanity");
            SanityChanged = true;
            BatteryPower = PlayerPrefs.GetFloat("BatteriesPower");
            Apples = PlayerPrefs.GetInt("ApplesCount");
            Batteries = PlayerPrefs.GetInt("BatteriesCount");
            ApplesLeft = PlayerPrefs.GetInt("ApplesLeftIG");
            BatteriesLeft = PlayerPrefs.GetInt("BatteriesLeftIG");
            AmmoLeft = PlayerPrefs.GetInt("AmmoLeftIG");
            SkinnyZombie = PlayerPrefs.GetInt("SkinnyZombieAlive");
            FastZombie = PlayerPrefs.GetInt("FastZombieAlive");
            FatZombie = PlayerPrefs.GetInt("FatZombieAlive");
            foreach (var weapon in Weapons)
            {
                if (PlayerPrefs.GetInt("Have" + weapon.GetTag()) == 1)
                {
                    weapon.SetActive(true);
                }
            }
            foreach (var key in Keys)
            {
                if (PlayerPrefs.GetInt("Have" + key.GetTag()) == 1)
                {
                    key.SetActive(true);
                }
            }
            if (PlayerPrefs.GetInt("FL") == 1)
            {
                FlashLight = true;
            }
            if (PlayerPrefs.GetInt("NV") == 1)
            {
                NVGoggles = true;
            }
            if (PlayerPrefs.GetInt("StartingAnimFinished") == 1)
            {
                StartingAnimationFinished = true;
            }
            if (PlayerPrefs.GetInt("PickedUpLighterSaved") == 1) 
            {
                PickedUpLighter = true;
            }
            if (PlayerPrefs.GetInt("ElectricityBoxDoorOpenedSaved") == 1)
            {
                ElectricityBoxDoorOpened = true;
            }
            if (PlayerPrefs.GetInt("PickedUpElectricityKeySaved") == 1)
            {
                PickedUpElectricityKey = true;
            }
            if (PlayerPrefs.GetInt("PickedUpWaterBucketSaved") == 1)
            {
                PickedUpWaterBucket = true;
            }
            if (PlayerPrefs.GetInt("FilledBucketWithWaterSaved") == 1)
            {
                FilledBucketWithWater = true;
            }
            if (PlayerPrefs.GetInt("ElectricityBoxExplodedSaved") == 1)
            {
                ElectricityBoxExploded = true;
            }
            if (PlayerPrefs.GetInt("OfficeDoorDestroyedSaved") == 1)
            {
                OfficeDoorDestroyed = true;
            }
            if (PlayerPrefs.GetInt("InOptionsMenuSaved") == 1)
            {
                InOptionsMode = false;
            }
            if(PlayerPrefs.GetInt("OpenedBedroomDoors") == 1)
            {
                OpenedBedroomDoors = true;
            }
            if (PlayerPrefs.GetInt("ElectricityBoxDoorOpened") == 1)
            {
                ElectricityBoxDoorOpened = true;
            }
            else ElectricityBoxDoorOpened = false;
            SavedGame = false;
        }
    }
}
