public class Weapon
{
    private readonly string Tag;
    private int Damage;
    private bool Active;

    public Weapon(string tag, int damage)
    {
        Tag = tag;
        Damage = damage;
        Active = false;
    }

    public string GetTag()
    {
        return Tag;
    }

    public int GetDamage()
    {
        return Damage;
    }

    public bool GetActive()
    {
        return Active;
    }

    public void SetActive(bool active)
    {
        Active = active;
    }

    public static int GetWeaponIndex(Weapon[] weapons, string tag)
    {
        for (var i = 0; i < weapons.Length; i++)
        {
            if (weapons[i].GetTag() == tag)
            {
                return i;
            }
        }
        return -1;
    }
}
