using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WeaponsLeft : MonoBehaviour
{
    [SerializeField] List<GameObject> Weapons;

    void Start()
    {
        StartCoroutine(CheckWeapons());
    }

    IEnumerator CheckWeapons()
    {
        yield return new WaitForSeconds(1);
        for (var i = 0; i < Weapons.Count; i++)
        {
            if (SaveScript.Weapons[i].GetActive())
            {
                Destroy(Weapons[i].gameObject);
            }
        }
    }
}
