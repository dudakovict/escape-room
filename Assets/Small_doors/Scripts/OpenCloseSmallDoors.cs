using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class OpenCloseSmallDoors : MonoBehaviour
{
    private Animator animator;
    private bool isOpen;
    private Outline outlineObj;
    private Camera mainCamera;
    public UIMessageSystem MessageSystem;


    // Start is called before the first frame update
    void Start()
    {
        isOpen = false;
        animator = GetComponent<Animator>();
        outlineObj = GetComponent<Outline>();
        mainCamera = Camera.main.GetComponent<Camera>();
    }

    // Update is called once per frame
    void Update()
    {
        /*
        // Currently moving an object
        if (SaveScript.HoldingObjectID != -1) return;

        Ray ray = mainCamera.ScreenPointToRay(Input.mousePosition);
        RaycastHit hit;
        
        if(Physics.Raycast(ray, out hit, 2f))
        //if (GetComponent<Collider>().Raycast(ray, out hit, 2f))
        {
            if (hit.transform.gameObject.GetInstanceID() != gameObject.GetInstanceID()) return;
            if (gameObject.tag == "Outlineable")
            {
                outlineObj.OutlineWidth = 10f;
                MessageSystem.DisableAll();
                MessageSystem.EnableCrosshair();
                MessageSystem.EnableSanity();

                if(!isOpen) MessageSystem.EnableOpenDoorMessage();
                else MessageSystem.EnableCloseDoorMessage();
            }

            if (Input.GetKeyDown(KeyCode.E))
            {
                if (isOpen == false)
                {
                    animator.SetTrigger("Open");
                    isOpen = true;
                    MessageSystem.DisableDoorMessage();
                }
                else if (isOpen == true)
                {
                    animator.SetTrigger("Close");
                    isOpen = false;
                    MessageSystem.DisableDoorMessage();
                }
            }
        }
        else
        {
            if (outlineObj.OutlineWidth != 0f)
                outlineObj.OutlineWidth = 0f;
        }
        */
    }

    public void HandleSmallDoors()
    {
        // Currently moving an object
        if (SaveScript.HoldingObjectID != -1) return;

        if (gameObject.tag == "Outlineable")
        {
            MessageSystem.DisableAll();
            MessageSystem.EnableCrosshair();
            MessageSystem.EnableSanity();
            if (!isOpen) MessageSystem.EnableOpenDoorMessage();
            else MessageSystem.EnableCloseDoorMessage();

            outlineObj.OutlineWidth = 10f;
        }

        if (Input.GetKeyDown(KeyCode.E))
        {
            if (isOpen == false)
            {
                animator.SetTrigger("Open");
                isOpen = true;
                MessageSystem.DisableDoorMessage();
            }
            else if (isOpen == true)
            {
                animator.SetTrigger("Close");
                isOpen = false;
                MessageSystem.DisableDoorMessage();
            }
        }
    }

    private void OnMouseExit()
    {
        if (Time.deltaTime == 0) return;

        outlineObj.OutlineWidth = 0f;
        MessageSystem.DisableDoorMessage();
        MessageSystem.EnableSanity();
        MessageSystem.EnableCrosshair();
    }
}
