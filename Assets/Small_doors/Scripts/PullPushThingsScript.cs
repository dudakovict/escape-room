using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PullPushThingsScript : MonoBehaviour
{
    private bool isInsideTrigger = false;
    public Animator animator;

    // tekst koji ce se igracu prikazati kad ne�to otvara
    public string OpenText = " Press E to open";

    // tekst koji ce se igracu prikazati kad ne�to zatvara
    public string CloseText = "Press E to close";

    private bool isOpen;

    void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Player")
        {
            isInsideTrigger = true;
        }
    }

    void OnTriggerExit(Collider other)
    {
        if (other.tag == "Player")
        {
            isInsideTrigger = false;
        }
    }

    // Start is called before the first frame update
    void Start()
    {
        isOpen = false;
        animator = GetComponent<Animator>();
    }

    // Update is called once per frame
    void Update()
    {
        if (isInsideTrigger)
        {
            if (Input.GetKeyDown(KeyCode.E))
            {
                if (isOpen == false)
                {
                    animator.SetTrigger("Pull");
                    isOpen = true;
                }
                else if (isOpen == true)
                {
                    animator.SetTrigger("Push");
                    isOpen = false;
                }
            }

        }
    }
}
