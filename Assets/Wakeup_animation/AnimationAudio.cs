using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AnimationAudio : MonoBehaviour
{

    [SerializeField] private AudioSource ear_ringing;
    [SerializeField] private AudioSource heartbeat;

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.P))
        {
            ear_ringing.Play();

            heartbeat.Play();
        }
    }
}
