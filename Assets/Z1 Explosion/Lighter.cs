using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Lighter : MonoBehaviour
{
    public bool isTrigger = false;

    public PlayerInventory playerInventory;

    public AudioSource take_item;

    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnTriggerEnter(Collider other) {
        isTrigger = true;
    }

    private void OnTriggerExit(Collider other) {
        isTrigger = false;
    }
   
}
