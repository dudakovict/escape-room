using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Valve : MonoBehaviour
{
    public UIMessageSystem MessageSystem;
    public bool isTrigger = false;
    public bool isTrigger_2 = false;

    public bool valve_loose = false;

    public PlayerInventory playerInventory;
    public Collider collider;
    public AudioSource open_valve;
    public AudioSource leak;

    private Outline outlineObj;
    private Camera mainCamera;


    // Start is called before the first frame update
    void Start()
    {
        outlineObj = GetComponent<Outline>();
        mainCamera = Camera.main.GetComponent<Camera>();

        gameObject.GetComponent<Exploder>().enabled = false;
        collider = GetComponent<Collider>();
    }

        IEnumerator myCoroutine(){
           yield return new WaitForSeconds(5);
           gameObject.GetComponent<Exploder>().enabled = true;
           leak.Stop();
        }

    // Update is called once per frame
    void Update()
    {
        Ray ray = mainCamera.ScreenPointToRay(Input.mousePosition);
        RaycastHit hit;

        if (Physics.Raycast(ray, out hit, 3f))
        {
            if (hit.transform.gameObject.GetInstanceID() != gameObject.GetInstanceID()) return;
            if (gameObject.tag == "Outlineable")
            {
                //outlineObj.OutlineColor = Color.green;
                outlineObj.OutlineWidth = 10f;
            }

            MessageSystem.DisableAll();
            MessageSystem.EnableSanity();

            if (!valve_loose) MessageSystem.EnableExplosionMessageLoose();
            else if(valve_loose && SaveScript.PickedUpLighter) MessageSystem.EnableExplosionMessageExplode();

            if (!valve_loose && Input.GetKeyDown(KeyCode.E))
            {
                MessageSystem.DisableExplosionMessageLoose();
                outlineObj.OutlineColor = Color.yellow;
                outlineObj.OutlineWidth = 10f;
                open_valve.Play();
                leak.Play();
                valve_loose = true;
            }

            if(valve_loose && SaveScript.PickedUpLighter && Input.GetKeyDown(KeyCode.X))
            {
                MessageSystem.DisableExplosionMessageExplode();
                
                outlineObj.OutlineColor = Color.red;
                outlineObj.OutlineWidth = 10f;

                StartCoroutine(myCoroutine());
                collider.enabled = false;
            }
        }
        else
        {
            //MessageSystem.EnableCrosshair();
            MessageSystem.DisableExplosionMessageLoose();
            MessageSystem.DisableExplosionMessageExplode();

            if (outlineObj.OutlineWidth != 0f)
                outlineObj.OutlineWidth = 0f;
        }
    }

    private void OnMouseExit()
    {
        MessageSystem.EnableCrosshair();
        MessageSystem.DisableExplosionMessageLoose();
        MessageSystem.DisableExplosionMessageExplode();

        outlineObj.OutlineWidth = 0f;
    }
}
